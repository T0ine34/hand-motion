
#allow a program to write an output in a file
output = "output"

with open(output, "w") as f:
    pass # do nothing, just create the file

def write(text):
    with open(output, "w") as f:
        f.write(text)
        
        
#the distance between two points using the euclidian distance
def distance(p1 : tuple[float,float], p2 : tuple[float,float]) -> float:
    return ((p1[0]-p2[0])**2 + (p1[1]-p2[1])**2)**0.5