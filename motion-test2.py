import cv2
import mediapipe as mp

import numpy as np

cap = cv2.VideoCapture(0)
mpHands = mp.solutions.hands
hands = mpHands.Hands()
mpDraw = mp.solutions.drawing_utils

#print the size of the image
print(cap.get(cv2.CAP_PROP_FRAME_WIDTH), cap.get(cv2.CAP_PROP_FRAME_HEIGHT))


from mouse import Mouse
import key as keyboard

TARGET_HAND = 0 #the hand that is tracked

from json import loads

output = "output"
output2 = "output2"
with open(output, "w") as f:
    pass # do nothing, just create the file

def write(text):
    with open(output, "w") as f:
        f.write(text)

#define drawings specs for the hands (red for the target hand, blue for all other hand)
target_hand_draw_spec = mpDraw.DrawingSpec(color=(0,0,255), thickness=2, circle_radius=2)
other_hand_draw_spec = mpDraw.DrawingSpec(color=(255,0,0), thickness=2, circle_radius=2)


#the distance between two points using the euclidian distance
def _distance(p1, p2):
    return ((p1[0]-p2[0])**2 + (p1[1]-p2[1])**2)**0.5

def get_scale(coords):
    return  _distance(coords[0], coords[5])/100 #for handle the distance between the hand and the camera

def dist_max(p1, p2, distance, scale): #return true if we respect the maximum distance (not too far)
    return _distance(p1, p2) <= distance*scale
def dist_min(p1, p2, distance, scale): #return true if we respect the minimum distance (not too close)
    return _distance(p1, p2) >= distance*scale

def higher_than(p1, p2, scale): #return true if p1 is higher than p2
    return p1[1] < p2[1]

def lower_than(p1, p2, scale): #return true if p1 is lower than p2
    return p1[1] > p2[1]

def left_of(p1, p2, scale): #return true if p1 is on the left of p2
    return p1[0] < p2[0]

def right_of(p1, p2, scale): #return true if p1 is on the right of p2
    return p1[0] > p2[0]

def vertically_aligned(p1, p2, scale, margin=60): #return true if p1 and p2 are vertically aligned
    return abs(p1[0] - p2[0]) <= margin*scale

def horizontally_aligned(p1, p2, scale, margin=60): #return true if p1 and p2 are horizontally aligned
    return abs(p1[1] - p2[1]) <= margin*scale



class ConfigFile:
    def  __init__(self, filename : str = "config.json"):
        self.data = {}
        try:
            with open(filename, "r") as f:
                self.data = loads(f.read())
        except FileNotFoundError:
            print("Config file not found")
        except Exception as e:
            print("Error while reading config file")
            print(e)
            
    def _test(self, key, coords, scale) -> bool:
        #return true if the current gesture is respected by the hand (coords, scale)
        if key >= len(self.data):
            raise KeyError("Key not found")
        gesture  = self.data[key]
        for point in gesture["points"]:
            p1 = coords[point["point1"]]
            p2 = coords[point["point2"]]
            if point["comparator_mode"] == "distance_max":
                if not dist_max(p1, p2, point["value"], scale):
                    #if we don't respect the maximum distance, we can stop here
                    return False
            elif point["comparator_mode"] == "distance_min":
                if not dist_min(p1, p2, point["value"], scale):
                    #if we don't respect the minimum distance, we can stop here
                    return False
            elif point["comparator_mode"] == "v_aligned":
                if not vertically_aligned(p1, p2, scale, point["value"] if "value" in point else 60):
                    #if we don't respect the vertical alignment, we can stop here
                    return False
            elif point["comparator_mode"] == "h_aligned":
                if not horizontally_aligned(p1, p2, scale, point["value"] if "value" in point else 60):
                    #if we don't respect the horizontal alignment, we can stop here
                    return False
            elif point["comparator_mode"] == "higher":
                if not higher_than(p1, p2, scale):
                    #if we don't respect the higher rule, we can stop here
                    return False
            elif point["comparator_mode"] == "lower":
                if not lower_than(p1, p2, scale):
                    #if we don't respect the lower rule, we can stop here
                    return False
            elif point["comparator_mode"] == "left":
                if not left_of(p1, p2, scale):
                    #if we don't respect the left rule, we can stop here
                    return False
            elif point["comparator_mode"] == "right":
                if not right_of(p1, p2, scale):
                    #if we don't respect the right rule, we can stop here
                    return False
            elif point["comparator_mode"] == "none":
                #if the comparator mode is not set, just ignore it
                pass
            else:
                raise ValueError('Unknown comparator mode "%s"'%point["comparator_mode"])
        #if we are here, it means that we respect all the points
        return True
            
    def what_is_it(self, coords, scale) -> str:
        #return the key of the gesture that is respected by the hand (coords, scale)
        for key in range(len(self.data)):
            if self.data[key]["label"] != "none" and self._test(key, coords, scale):
                return self.data[key]["label"]
        return "none"

    def get_labels(self):
        return [gesture["label"] for gesture in self.data]
    
    def get_color(self, label):
        for gesture in self.data:
            if gesture["label"] == label:
                return gesture["color"]
        return None


    
    
def is_hand_right(coords):
    return coords[5][0] < coords[13][0]


TRACKED_HAND = 0
MARGIN = 128 #the margin around the hand that is tracked
SCREEN_SIZE = (1920, 1080) #the size of the screen

mouse = Mouse()

color = (0, 0, 255)

cooldown = 0


config_file = ConfigFile()
print("config file loaded")

hand_pos_history = {}

def going_up(id_hand, hand_coords):
    #compare the current position of the hand with the previous one
    return hand_coords[1] < hand_pos_history[id_hand][1]

def going_down(id_hand, hand_coords):
    #compare the current position of the hand with the previous one
    return hand_coords[1] > hand_pos_history[id_hand][1]

def going_left(id_hand, hand_coords):
    #compare the current position of the hand with the previous one
    return hand_coords[0] < hand_pos_history[id_hand][0]

def going_right(id_hand, hand_coords):
    #compare the current position of the hand with the previous one
    return hand_coords[0] > hand_pos_history[id_hand][0]

try:
    while True:
        
        if cooldown > 0:
            cooldown -= 1
        
        success, image = cap.read()
        
        #mirror the image
        image = cv2.flip(image, 1)
        
        imageRGB = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        results = hands.process(imageRGB)
        
        #create a white image of the same size as the original image
        image2 = np.zeros(image.shape, np.uint8)
        
        h, w, c = image.shape
        
        state = "none"
        # checking whether a hand is detected
        if results.multi_hand_landmarks:
            for id_hand, handLms in enumerate(results.multi_hand_landmarks[::-1]): # working with each hand
                
                coords = [(int(lm.x * w), int(lm.y * h)) for lm in handLms.landmark]

                scale = get_scale(coords)
                
                    
                state = config_file.what_is_it(coords, scale)
                write(state)
                
                #set the color of the circle to green if the fingers are joined, red otherwise
                color = config_file.get_color(state)
                    
                #draw the circle
                cv2.circle(image2, coords[5], int(25*scale), color, cv2.FILLED)
                
                hand_coords = list(coords[5]) #the coordinates of the center of the hand

                if state == "hand open":
                    cooldown = 0

                if state =="fist closed" and not mouse.grabbed:
                    mouse.grab()
                elif state == "hand open" and mouse.grabbed:
                    mouse.release()
                elif state == "two fingers up":
                    if is_hand_right(coords) and cooldown == 0:
                        keyboard.press("right")
                        cooldown = 10
                    elif cooldown == 0:
                        cooldown = 10
                        keyboard.press("left")
                elif state == "two fingers side" and id_hand != TRACKED_HAND: #if the hand is not the tracked hand
                    if going_up(id_hand, hand_coords):
                        mouse.scroll(50)
                    elif going_down(id_hand, hand_coords):
                        mouse.scroll(-50)
                
                
                if id_hand == TRACKED_HAND:
                    
                    #convert the coordinates to the size of the screen, plus a margin of 50 pixels
                    hand_coords[0] = (hand_coords[0] * (SCREEN_SIZE[0]+2*MARGIN) / 640)
                    hand_coords[1] = (hand_coords[1] * (SCREEN_SIZE[1]+2*MARGIN) / 480)
                    
                    #if the hand is in the margin, set the coordinates to the margin
                    if hand_coords[0] < MARGIN: #the hand is too far to the left
                        hand_coords[0] = 0 #set the x coordinate to 0
                    elif hand_coords[0] > SCREEN_SIZE[0] + MARGIN: #the hand is too far to the right
                        hand_coords[0] = SCREEN_SIZE[0]-1 #set the x coordinate to 1920
                    else: #the hand is in the margin
                        hand_coords[0] -= MARGIN #set the x coordinate to the margin
                    
                    if hand_coords[1] < MARGIN: #the hand is too far to the top
                        hand_coords[1] = 0 #set the y coordinate to 0
                    elif hand_coords[1] > SCREEN_SIZE[1] + MARGIN: #the hand is too far to the bottom
                        hand_coords[1] = SCREEN_SIZE[1]-1 #set the y coordinate to 1080
                    else: #the hand is in the margin
                        hand_coords[1] -= MARGIN #set the y coordinate to the margin
                    
                    mouse.move(hand_coords[0], hand_coords[1])
                    
                    #draw the targeted hand
                    mpDraw.draw_landmarks(image, handLms, mpHands.HAND_CONNECTIONS, target_hand_draw_spec)
                else:
                    #draw the other hand
                    mpDraw.draw_landmarks(image, handLms, mpHands.HAND_CONNECTIONS, other_hand_draw_spec)

                #set the hand position history, so that we can compare the current position with the previous one
                hand_pos_history[id_hand] = hand_coords

                
        cv2.putText(image, "targeted hand color : red" , (10, 30), cv2.FONT_HERSHEY_PLAIN, 1, (0, 0, 255), 2)
        cv2.imshow("Output", image2)
        cv2.imshow("Input", image)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        

except Exception as e:
    print(e)

finally:
    if mouse.grabbed:
        mouse.release()

    cap.release()
    cv2.destroyAllWindows()