import cv2
import time
import math as m
import mediapipe as mp

mpDraw = mp.solutions.drawing_utils

mpHands = mp.solutions.hands
hands = mpHands.Hands()



# Initialize mediapipe pose class.
mp_pose = mp.solutions.pose
pose = mp_pose.Pose()

# For webcam input replace file name with 0.
file_name = 0
cap = cv2.VideoCapture(file_name)

# Meta.
fps = int(cap.get(cv2.CAP_PROP_FPS))
width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
frame_size = (width, height)

#display the video
cv2.namedWindow("Video", cv2.WINDOW_NORMAL)

while True:

	# Capture frames.
	success, image = cap.read()
	if not success:
		print("Null.Frames")
		break

	# Get fps.
	fps = cap.get(cv2.CAP_PROP_FPS)
	# Get height and width of the frame.
	h, w = image.shape[:2]

	# Convert the BGR image to RGB.
	image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

	# Process the image.
	keypoints = pose.process(image)
	hands_results = hands.process(image)

	# Convert the image back to BGR.
	image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
 
	# Draw the pose annotation on the image.
	if keypoints.pose_landmarks:
		mpDraw.draw_landmarks(image, keypoints.pose_landmarks, mp_pose.POSE_CONNECTIONS)
  
	if hands_results.multi_hand_landmarks:
		for id_hand, handLms in enumerate(hands_results.multi_hand_landmarks[::-1]): # working with each hand
			mpDraw.draw_landmarks(image, handLms, mpHands.HAND_CONNECTIONS, mpDraw.DrawingSpec(color=(0,0,255), thickness=2, circle_radius=2))
 
	#display the video
	cv2.imshow("Video", image)

	#the stop condition
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break


#release the capture
cap.release()
cv2.destroyAllWindows()