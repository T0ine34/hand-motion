# TechVidvan hand Gesture Recognizer

# import necessary packages

import cv2
import numpy as np
import mediapipe as mp
import tensorflow as tf
from tensorflow.keras.models import load_model

from sys import argv
SHOW = "show" in argv

from key import *

# initialize mediapipe
mpHands = mp.solutions.hands
hands = mpHands.Hands(max_num_hands=2, min_detection_confidence=0.7)
mpDraw = mp.solutions.drawing_utils

# Load the gesture recognizer model
model = load_model('mp_hand_gesture')

# Load class names
f = open('gesture.names', 'r')
classNames = f.read().split('\n')
f.close()
print(classNames)

def write(text):
    with open(output, "w") as f:
        f.write(text)

# Initialize the webcam
cap = cv2.VideoCapture(0)


output = "output"
with open(output, "w") as f:
    pass # do nothing, just create the file
current_gesture = None
last_action = None

while True:
    # Read each frame from the webcam
    _, frame = cap.read()

    x, y, c = frame.shape

    # Flip the frame vertically
    frame = cv2.flip(frame, 1)
    framergb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

    # Get hand landmark prediction
    result = hands.process(framergb)

    # print(result)
    
    classNamesList = []

    # post process the result
    if result.multi_hand_landmarks:
        landmarks = []
        for handslms in result.multi_hand_landmarks:
            for lm in handslms.landmark:
                lmx = int(lm.x * x)
                lmy = int(lm.y * y)
                landmarks.append([lmx, lmy])
                
            if SHOW:
                mpDraw.draw_landmarks(frame, handslms, mpHands.HAND_CONNECTIONS)
                
            #predict the gesture
            predictions = model.predict(np.array([landmarks], dtype='float32'))
            
            classID = np.argmax(predictions[0])
            classNamesList.append(classNames[classID])
            
    if SHOW:
        #cv2.putText(frame, className, (10, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
        pass
    
    
    print(classNamesList)
    # if className == "thumbs up" and last_action != "thumbs up":
    #     press("right")
    #     write("right")
    #     last_action = "thumbs up"
    # elif className == "thumbs down" and last_action != "thumbs down":
    #     press("left")
    #     write("left")
    #     last_action = "thumbs down"
    # elif className == "stop" or className == "live long":
    #     break
    # elif className == "":
    #     last_action = None
    #     write("")
        
    # Show the final output
    if SHOW:
        cv2.imshow("Frame", frame)
        if cv2.waitKey(1) == ord('q'):
            break
        
cap.release()

if SHOW:
    cv2.destroyAllWindows()