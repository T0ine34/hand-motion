#allow a program to control the mouse
import pyautogui


pyautogui.FAILSAFE = False

from time import sleep

class Mouse:
    instance = None
    def __init__(self):
        if Mouse.instance == None:
            Mouse.instance = self
        else:
            raise Exception("Cannot create another Mouse object")
        self.grabbed = False
        
    
    def grab(self, button = "left"):
        #grabs the mouse
        pyautogui.mouseDown(button = button)
        self.grabbed = True
        
    def release(self, button = "left"):
        #releases the mouse
        pyautogui.mouseUp(button = button)
        self.grabbed = False
        
    def move(self, x, y):
        #moves the mouse to the specified position
        pyautogui.moveTo(x, y)
        
    def move_rel(self, x, y):
        #moves the mouse relative to its current position
        pyautogui.moveRel(x, y)
        
    def scroll(self, amount):
        #scrolls the mouse wheel
        pyautogui.scroll(amount)
        
    def click(self, button = "left"):
        #clicks the mouse
        pyautogui.click(button = button)
        
    def double_click(self, button = "left"):
        #double clicks the mouse
        pyautogui.doubleClick(button = button)
        
        
        
    def __del__(self):
        Mouse.instance = None
        
        
if __name__ == "__main__":
    mouse = Mouse()
    for i in range(10):
        mouse.scroll(10)
    for i in range(10):
        mouse.scroll(-10)