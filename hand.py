import mediapipe as mp
#mpDraw = mp.solutions.drawing_utils
#mpHands = mp.solutions.hands

import json

import math as m



class Coord:
    """
    mode : "absolute" or "relative"
    """
    def __init__(self, value : float, mode : str = "absolute"):
        if mode not in ("absolute", "relative"):
            raise ValueError("Invalid mode")
        self.mode = mode == "absolute"
        
        if not self.mode and (value < 0 or value > 1):
            raise ValueError("Invalid value")
        self.value = value

    def get_value(self, max_value : float = -1) -> float:
        if self.mode:
            return self.value
        else:
            if max_value == -1:
                raise ValueError("max_value is required when mode is relative")
            return self.value * max_value

    
class Relation:
    """
    A class to represent an reation between 2 points
    """
    def __init__(self, condition : str):
        tokens = condition.strip().split()
        
        if len(tokens) not in (3, 5):
            raise ValueError("Invalid condition")
        
        if tokens[0].startswith("["):
            #the user give an index
            if tokens[0].endswith("]"):
                self.index1 = int(tokens[0][1:-1])
            else:
                raise ValueError("Invalid condition '%s'" % condition)
        else:
            #the user give a constant (absolute or percentage)
            subtokens = tokens[0].split(":")
            if len(subtokens) == 1:
                self.index1 = float(subtokens[0])
            elif len(subtokens) == 2:
                if subtokens[1] == "absolute":
                    
    

class Gesture:
    
    @staticmethod
    def from_JSON(json : str|dict):
        if type(json) == str:
            json = json.loads(json)
        return Gesture(json["name"], json["conditions"])
    
    
if __name__ == "__main__":
    r = Relation("[0] below [5]")